#!/bin/bash

date=`date +%s`
mongodump -u $2 -p $3 -d $4 -o $HOME/dumps/dump.$date
cd $HOME/dumps
git config credential.helper store
git checkout -b daily-$1
git add .
git commit -m $date
git push --set-upstream origin daily-$1
