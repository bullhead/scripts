#!/bin/bash

cd ~/target/
mv $1.war api.war
cp api.war /opt/tomcat/webapps/
chown tomcat /opt/tomcat/webapps/api.war
systemctl restart tomcat
